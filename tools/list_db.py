import argparse
import asyncio
import os
import aiopg


async def run_query_results(dsn: str, query: str, params: tuple = None):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query, params)
                return [row async for row in cur]


async def main(dsn: str):
    records = await run_query_results(dsn=dsn, query="SELECT * FROM availability")
    print("\n".join(str(r) for r in records))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Lists database table (password is read from DB_PASSWORD environment variable)"
    )
    parser.add_argument("-H", "--dbhost", help="DB host", required=True)
    parser.add_argument("-u", "--dbuser", help="DB user", required=True)
    parser.add_argument("-d", "--dbname", help="DB name", required=True)
    parser.add_argument("-p", "--dbport", help="DB port", type=int, required=True)
    args = parser.parse_args()

    dbpassword = os.environ["DB_PASSWORD"]
    dsn = f"dbname={args.dbname} user={args.dbuser} password={dbpassword} host={args.dbhost} port={args.dbport}"

    asyncio.run(main(dsn))
