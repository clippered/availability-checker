import argparse
import asyncio
import os
import aiopg


DROP_QUERY = "DROP TABLE IF EXISTS availability"
CREATE_QUERY = """CREATE TABLE IF NOT EXISTS availability (
    id              SERIAL NOT NULL,
    url           TEXT NOT NULL,
    request_time     TIMESTAMP NOT NULL,
    error         VARCHAR(8) NULL,
    status        SMALLINT NULL,
    duration  INTERVAL NULL,
    pattern        TEXT NULL,
    pattern_found  BOOL NULL
)"""


async def run_query(dsn: str, query: str):
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query)


async def main(dsn: str):
    await run_query(dsn=dsn, query=DROP_QUERY)
    print("Table dropped")
    await run_query(dsn=dsn, query=CREATE_QUERY)
    print("Table created")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Recreates database table (password is read from DB_PASSWORD environment variable)"
    )
    parser.add_argument("-H", "--dbhost", help="DB host", required=True)
    parser.add_argument("-u", "--dbuser", help="DB user", required=True)
    parser.add_argument("-d", "--dbname", help="DB name", required=True)
    parser.add_argument("-p", "--dbport", help="DB port", type=int, required=True)
    args = parser.parse_args()

    dbpassword = os.environ["DB_PASSWORD"]
    dsn = f"dbname={args.dbname} user={args.dbuser} password={dbpassword} host={args.dbhost} port={args.dbport}"

    asyncio.run(main(dsn))
