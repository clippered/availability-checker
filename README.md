# Website Availability Checker

A simple website availability checking program which pings a set of websites to record their response times.

Requirements:
* A Kafka cluster must be created first and topic created too
* A Postgres database must be first and database created too

## [Producer](producer/)

This service periodically checks a list of websites an input csv file, and forwards necesssary data to a Kafka topic. To run:
```
availability-producer -k <kafka-server> -t <kafka-topic> --kafkacafile <kafka-cafile> --kafkacertfile <kafka-certfile> --kafkakeyfile <kafka-keyfile> -T <timeout-value> <websites-file>
```
The `kafka-server` is in format of `host:port`. The `kafka-topic` value is `availability` by default. The `timeout-value` unit is in seconds and is `30` seconds by default.

The `websites-file` is in CSV format without header and has 2 columns: a required url and an optional regex pattern. An example can be found in this [link](producer/example/websites.csv)

This program was written using Python-3.8. To install:
```
virtualenv -p 3.8 env
. env/bin/activate
cd producer
pip install -e .
```

As for deploying, it uses `cron` to periodically run the `availability-producer` program. To build and run the container:
```
cd producer
docker build -t producer .
# put certificates in `cert` folder
docker run -it --rm  -v /$PWD/cert:/app/cert -e KAFKA_SERVER=<kafka_server> -e KAFKA_CAFILE=/app/cert/ca.pem -e KAFKA_CERTFILE=/app/cert/service.cert -e KAFKA_KEYFILE=/app/cert/service.key -e KAFKA_TOPIC=availability -e WEBSITES=/app/example/websites.txt producer
```

## [Consumer](consumer/)

This service reads from a Kafka topic and forwards the data to a Postgres database. This runs indefinitely until terminated. It also requires the database table has been created first, see next section. To run:
```
DB_PASSWORD=<pg-password> availability-consumer -k <kafka-server> -t <kafka-topic> --kafkacafile <kafka-cafile> --kafkacertfile <kafka-certfile> --kafkakeyfile <kafka-keyfile> -H <pg-host> -p <pg-port> -u <pg-user> -d <pg-database>
```
The `kafka-server` is in format of `host:port`. The `kafka-topic` value is `availability` by default. The `DB_PASSWORD` is the environment variable containing the database password.

This program was written using Python-3.8. To install:
```
virtualenv -p 3.8 env
. env/bin/activate
cd consumer
pip install -e .
```

To build and run the container:
```
cd consumer
docker build -t consumer .
# put certificates in `cert` folder
docker run -it --rm  -v /$PWD/cert:/app/cert -e DB_PASSWORD=<pg_password> consumer -k <kafka_server> --kafkacafile /app/cert/ca.pem --kafkacertfile /app/cert/service.cert --kafkakeyfile /app/cert/service.key -t availability -H <pg_host> -p <pg_port> -u <pg_user> -d <pg_db>
```

### [Tools](tools/)

Before records can be put into database table, the table needs to be created first. To create the table:
```
DB_PASSWORD=<pg_password> python tools/recreate_db.py -H <pg_host> -p <pg_port> -u <pg_user> -d <pg_db>
```

And to check the table contents:
```
DB_PASSWORD=<pg_password> python tools/list_db.py -H <pg_host> -p <pg_port> -u <pg_user> -d <pg_db>
```
