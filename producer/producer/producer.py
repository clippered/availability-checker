import logging
import argparse
import re
import json
import time
import ssl
import sys
import aiohttp
import asyncio
from aiokafka import AIOKafkaProducer


async def fetch_url(session: aiohttp.ClientSession, url: str) -> tuple:
    async with session.get(url) as response:
        html = await response.text()
        status = response.status
    return status, html


async def check_website(
    session: aiohttp.ClientSession, url: str, pattern: str = None
) -> dict:
    data = {"url": url, "error": None}
    logging.info(f"Fetching `{url}`")
    try:
        start_time = time.time()
        data["request_time"] = start_time
        status, html = await fetch_url(session=session, url=url)
        elapsed_time = time.time() - start_time
        data["duration"] = elapsed_time
        data["status"] = status

        data["pattern"] = pattern
        if pattern:
            match = re.search(pattern, html)
            data["pattern_found"] = match is not None
    except asyncio.TimeoutError:
        data["error"] = "timeout"
        logging.error(f"Timeout occured when fetching `{url}`")
    except Exception:
        logging.exception(f"Exception occured when fetching `{url}`")
        data["error"] = "error"
    return data


async def publish_message(producer: AIOKafkaProducer, topic: str, message: bytes):
    try:
        await producer.send_and_wait(topic, message)
    except Exception as ex:
        logging.exception(ex)


async def check_website_availability(
    session: aiohttp.ClientSession,
    producer: AIOKafkaProducer,
    topic: str,
    url: str,
    pattern: str = None,
):
    data = await check_website(session=session, url=url, pattern=pattern)
    message = json.dumps(data).encode("utf-8")
    await publish_message(producer=producer, topic=topic, message=message)


async def check_websites(
    websites: list,
    timeout: int,
    kafka_server: str,
    kafka_topic: str,
    kafka_cafile: str,
    kafka_certfile: str,
    kafka_keyfile: str,
):
    logging.info("Checking availability of websites")

    ssl_ctx = ssl.create_default_context(cafile=kafka_cafile)
    ssl_ctx.load_cert_chain(certfile=kafka_certfile, keyfile=kafka_keyfile)

    async with aiohttp.ClientSession(
        timeout=aiohttp.ClientTimeout(total=timeout)
    ) as session:
        producer = AIOKafkaProducer(
            bootstrap_servers=kafka_server,
            security_protocol="SSL",
            ssl_context=ssl_ctx,
        )
        await producer.start()
        try:
            tasks = [
                asyncio.create_task(
                    check_website_availability(
                        session=session,
                        producer=producer,
                        topic=kafka_topic,
                        url=url,
                        pattern=pattern,
                    )
                )
                for url, pattern in websites
            ]
            await asyncio.gather(*tasks)
        finally:
            await producer.stop()


def main():
    # read from env
    parser = argparse.ArgumentParser(description="Availability Producer")
    parser.add_argument("websites_file", help="file with list of websites")
    parser.add_argument(
        "-T", "--timeout", help="Request timeout value in seconds", type=int, default=30
    )
    parser.add_argument("-k", "--kafkaserver", help="Kafka server", required=True)
    parser.add_argument(
        "-t", "--kafkatopic", help="Kafka topic", default="availability"
    )
    parser.add_argument("--kafkacafile", help="Kafka cafile", default="cert/ca.pem")
    parser.add_argument(
        "--kafkacertfile", help="Kafka certfile", default="cert/service.cert"
    )
    parser.add_argument(
        "--kafkakeyfile", help="Kafka keyfile", default="cert/service.key"
    )
    args = parser.parse_args()

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    with open(args.websites_file) as f:
        websites = [ws.split(",") for ws in f.readlines() if ws.strip()]
        websites = [
            (ws[0].strip(), ws[1].strip() if len(ws) > 1 else None) for ws in websites
        ]
    if args.timeout <= 0 or args.timeout >= 60:
        raise ValueError("Timeout should be greater than 0 and less than 60")

    asyncio.run(
        check_websites(
            websites=websites,
            timeout=args.timeout,
            kafka_server=args.kafkaserver,
            kafka_topic=args.kafkatopic,
            kafka_cafile=args.kafkacafile,
            kafka_certfile=args.kafkacertfile,
            kafka_keyfile=args.kafkakeyfile,
        )
    )


if __name__ == "__main__":
    main()
