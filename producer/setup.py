from os.path import join, dirname
from setuptools import setup, find_packages


with open(join(dirname(__file__), "requirements.txt")) as f:
    requirements = [line.strip() for line in f.readlines() if line.strip()]

setup(
    name="availability-producer",
    version="0.0.1",
    description="Availability Producer",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=requirements,
    entry_points={"console_scripts": ["availability-producer=producer.producer:main"]},
)
