#!/bin/bash

set -ex
source /app/env.sh
printenv
/usr/local/bin/availability-producer -k $KAFKA_SERVER -t $KAFKA_TOPIC --kafkacafile $KAFKA_CAFILE --kafkacertfile $KAFKA_CERTFILE --kafkakeyfile $KAFKA_KEYFILE $WEBSITES
