import logging
import argparse
import json
import ssl
import os
import sys
import asyncio
from aiokafka import AIOKafkaConsumer
import aiopg


INSERT_QUERY = """INSERT INTO availability (
    url, request_time, error, status, duration, pattern, pattern_found
) VALUES(
    %s, to_timestamp(%s), %s, %s, make_interval(secs := %s), %s, %s
) RETURNING id"""


async def run_query_results(dsn: str, query: str, params: tuple = None) -> list:
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query, params)
                return [row async for row in cur]


async def consume_availability_logs(
    dsn: str,
    kafka_server: str,
    kafka_topic: str,
    kafka_cafile: str,
    kafka_certfile: str,
    kafka_keyfile: str,
):
    logging.info("Reading availability log events")

    ssl_ctx = ssl.create_default_context(cafile=kafka_cafile)
    ssl_ctx.load_cert_chain(certfile=kafka_certfile, keyfile=kafka_keyfile)
    consumer = AIOKafkaConsumer(
        kafka_topic,
        bootstrap_servers=kafka_server,
        security_protocol="SSL",
        ssl_context=ssl_ctx,
    )
    await consumer.start()
    try:
        # waits and consumes a message
        async for msg in consumer:
            logging.info(f"Reading #{msg.offset} {msg.timestamp}: {msg.value}")
            data = json.loads(msg.value)
            params = (
                data["url"],
                data["request_time"],
                data.get("error"),
                data.get("status"),
                data.get("duration"),
                data.get("pattern"),
                data.get("pattern_found"),
            )
            result = await run_query_results(dsn=dsn, query=INSERT_QUERY, params=params)
            logging.info(f"message #{msg.offset} as row #{result[0]}")
    except Exception:
        logging.exception("Exception occured")
    finally:
        await consumer.stop()


def main():
    parser = argparse.ArgumentParser(
        description="Availability Consumer (password is read from DB_PASSWORD environment variable)"
    )
    parser.add_argument("-H", "--dbhost", help="DB host", required=True)
    parser.add_argument("-u", "--dbuser", help="DB user", required=True)
    parser.add_argument("-d", "--dbname", help="DB name", required=True)
    parser.add_argument("-p", "--dbport", help="DB port", type=int, required=True)
    parser.add_argument("-k", "--kafkaserver", help="Kafka server", required=True)
    parser.add_argument(
        "-t", "--kafkatopic", help="Kafka topic", default="availability"
    )
    parser.add_argument("--kafkacafile", help="Kafka cafile", default="cert/ca.pem")
    parser.add_argument(
        "--kafkacertfile", help="Kafka certfile", default="cert/service.cert"
    )
    parser.add_argument(
        "--kafkakeyfile", help="Kafka keyfile", default="cert/service.key"
    )
    args = parser.parse_args()

    dbpassword = os.environ["DB_PASSWORD"]
    dsn = f"dbname={args.dbname} user={args.dbuser} password={dbpassword} host={args.dbhost} port={args.dbport}"

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    asyncio.run(
        consume_availability_logs(
            dsn=dsn,
            kafka_server=args.kafkaserver,
            kafka_topic=args.kafkatopic,
            kafka_cafile=args.kafkacafile,
            kafka_certfile=args.kafkacertfile,
            kafka_keyfile=args.kafkakeyfile,
        )
    )


if __name__ == "__main__":
    main()
